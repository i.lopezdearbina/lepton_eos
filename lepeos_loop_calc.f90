program lepeos_loop_cal
  use,intrinsic::iso_fortran_env, wp=>real64
  use omp_lib
  use constants
  use lepeos_mod
  use root_finder_mod
  implicit none
  real(wp) :: mue_min, mue_max, dmue
  real(wp) :: ntot, npar, nant
  real(wp) :: ptot, ppar, pant
  real(wp) :: utot, upar, uant
  real(wp) :: stot, spar, sant
  real(wp) :: rho, temp, cheme, nb, yl_in, dmul, mul, mlepmev, yl
  integer  :: i, j, k, iounit, istat
  integer  :: nrho, ntemp, nyl, nmu, num_threads
  logical  :: muons
  real(dp) :: rho_max, rho_min, temp_max, temp_min, yl_max, yl_min, mul_min, mul_max
  real(dp), allocatable, dimension(:)     :: rhoarray, tarray, ylarray, mularray
  real(dp), allocatable, dimension(:,:,:) :: parray, earray, muearray, muparray  
  real(dp), allocatable, dimension(:,:,:) :: munarray, dummy, garray, sarray

  !--------------------------------------------------------------------
  ! read namelist
  !--------------------------------------------------------------------
  namelist /control/ muons
  namelist /table/ nrho, ntemp, nyl, nmu, &
                   rho_max, rho_min,      &
                   temp_max, temp_min,    &
                   yl_max, yl_min,        &
                   mul_max, mul_min

  ! open and read 'control' namelist
  open(newunit=iounit, file='input_list.nml', status='old', &
           action='read', form='formatted', access='sequential')
  read(iounit, nml=control)
  close(iounit)
  
  ! open and read 'table' namelist
  open(newunit=iounit, file='input_list.nml', status='old', &
                action='read', form='formatted', access='sequential')
  read(iounit, nml=table)
  close(iounit)
  
  print'(":: Electron number calculator for arbitrary degenerate and relativistic gas")'
  

  mlepmev = memev
  if (muons) mlepmev = mmumev
  
        
  do
    print'(":: Enter rho (g/cm^3), temp (MeV), Yl: ")'
    read(5,*) rho, temp, yl
    nb = rho/amu * 1.0e-39_dp
    if (yl == 0.0_dp) then
       mul = 0.0_dp
    else
       call root_newton (10.d0, newton_lepton,  &
         [mlepmev, temp, yl*nb], mul, istat)
       mul = temp * mul + mlepmev
    end if
    
    call abundlep (mlepmev, temp, mul, ntot, npar, nant)
    call preslep  (mlepmev, temp, mul, ptot, ppar, pant)
    call enerlep  (mlepmev, temp, mul, utot, upar, uant)
    stot = (ptot + utot + mul * ntot) / temp / nb
    print'(5x,"rho",10x,"temp",10x,"Yl",12x,"mul",9x,"ptot",9x,"utot",9x,"stot")'
    write(*,'(7(1x,es12.5))') rho, temp, ntot/nb, mul, ptot, utot, stot
  end do
end program lepeos_loop_cal
