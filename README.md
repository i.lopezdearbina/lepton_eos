# Lepton EOS

Computes the equation of state (EOS) of an arbitrarily degenerate and
relativistic lepton gas.

Two modes of using it:

1. Calculator mode: the program asks the user to introduce a density (in
   g/cm^3), a temperatur (in MeV) and Yl (lepton fraction). The program
   performs the calculation and prints it on the standard output and returns to
   ask the user to introduce new input values in an infinite loop (Ctrl+C to
   kill it).

   Make it:
   ```
   make lepeos_loop_calc
   ```

2. Table mode: creates a 3D table for the input values given in `input_list.nml`
   for a logarithmic space density and temperature grid and linear lepton
   fraction grid.
   Make it:
   ```
   make main
   ```

To chose between electron or muon (or easily upgraded to tau) set the logical
variable in `control` input list `muons` to `.false.` for electrons or `.true.`
for muons.


## Method
The codes uses a Newton-Rapshon iteration scheme to determine the chemical
poential assocated to the give lepton fraction.
