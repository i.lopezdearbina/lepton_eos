program main
  use,intrinsic::iso_fortran_env, wp=>real64
  use omp_lib
  use constants
  use lepeos_mod
  use root_finder_mod
  implicit none
  real(wp) :: mue_min, mue_max, dmue
  real(wp) :: ntot, npar, nant
  real(wp) :: ptot, ppar, pant
  real(wp) :: utot, upar, uant
  real(wp) :: stot, spar, sant
  real(wp) :: rho, temp, cheme, nb, yl_in, dmul, mul, mlepmev, yl
  integer  :: i, j, k, iounit, istat
  integer  :: nrho, ntemp, nyl, nmu, num_threads
  logical  :: elect, muons
  real(dp) :: rho_max, rho_min, temp_max, temp_min, yl_max, yl_min, mul_min, mul_max
  real(dp), allocatable, dimension(:)     :: rhoarray, tarray, ylarray
  real(dp), allocatable, dimension(:,:,:) :: parray, earray, mularray, sarray

  !--------------------------------------------------------------------
  ! read namelist
  !--------------------------------------------------------------------
  namelist /control/ elect, muons
  namelist /table/ nrho, ntemp, nyl, nmu, &
                   rho_max, rho_min,      &
                   temp_max, temp_min,    &
                   yl_max, yl_min,        &
                   mul_max, mul_min

  ! open and read 'control' namelist
  open(newunit=iounit, file='input_list.nml', status='old', &
           action='read', form='formatted', access='sequential')
  read(iounit, nml=control)
  close(iounit)
  
  ! open and read 'table' namelist
  open(newunit=iounit, file='input_list.nml', status='old', &
                action='read', form='formatted', access='sequential')
  read(iounit, nml=table)
  close(iounit)

  allocate(rhoarray(nrho), tarray(ntemp), ylarray(nyl))
  allocate(parray(nrho,ntemp,nyl),  &
           earray(nrho,ntemp,nyl),  &
           mularray(nrho,ntemp,nyl),&
           sarray(nrho,ntemp,nyl)   )
  
  print'(":: Electron number calculator for arbitrary degenerate and relativistic gas")'
  

  if (elect) mlepmev = memev
  if (muons) mlepmev = mmumev
  
  ! table arrays
  if (ntemp < 2) then
    tarray(:) = temp_min
  else
    tarray   = [ (temp_min*10.0_dp**((i-1)*(log10(temp_max)-log10(temp_min))/(ntemp-1)), i=1, ntemp) ]
  end if
  if (nrho < 2) then
    rhoarray(:) = rho_min
  else
    rhoarray = [ (rho_min *10.0_dp**((i-1)*(log10(rho_max)-log10(rho_min))/(nrho-1)), i=1, nrho) ]
  end if
  if (nyl < 2) then
    ylarray(:) = yl_min
  else
    ylarray  = [ (yl_min + (i-1)*(yl_max-yl_min)/(nyl-1), i=1, nyl) ]
  end if

  print'(":: OMP_MAX_THREADS() ",g0)', OMP_GET_MAX_THREADS()
  !$OMP PARALLEL DO NUM_THREADS(OMP_GET_MAX_THREADS()) &
  !$OMP PRIVATE(ntot, npar, nant, ptot, ppar, pant, utot, upar, uant, stot, spar, sant) &
  !$OMP PRIVATE(rho, temp, mul, nb, yl, istat, i, j, k)

  yl_loop: do k=1, nyl
  yl = ylarray(k)

    ! loop for temperature
    temp_loop: do j=1, ntemp
      temp = tarray(j)
  
      ! loop for densities
      rho_loop: do i=1, nrho
      rho = rhoarray(i)
      nb = rho/amu * 1.0e-39_dp

        
        print'(" Number threads: ",g0," thread number ",g0)', &
          OMP_GET_NUM_THREADS(), OMP_GET_THREAD_NUM()
        
        call root_newton (10.d0, newton_lepton,  &
             [mlepmev, temp, yl*nb], mul, istat)
        mul = temp * mul + mlepmev
        
        call abundlep (mlepmev, temp, mul, ntot, npar, nant)
        call preslep  (mlepmev, temp, mul, ptot, ppar, pant)
        call enerlep  (mlepmev, temp, mul, utot, upar, uant)
        stot = (ptot + utot + mul * ntot) / temp / nb
        parray(i,j,k)   = ptot
        earray(i,j,k)   = utot
        mularray(i,j,k) = mul
        sarray(i,j,k)   = stot

        !write(*,*) rho, temp, ntot/nb, mul, ptot, utot, stot
        !write(iounit,'(7(2x,es12.6))') &
        !  rho, temp, ntot/nb, mul, ptot, utot, stot

      end do rho_loop
    end do temp_loop
  end do yl_loop
  !$OMP END PARALLEL DO
  
  open(newunit=iounit, file='lepeos.dat')
  yl_write_loop: do k=1, nyl
    temp_write_loop: do j=1, ntemp
      rho_write_loop: do i=1, nrho
        write(iounit,*) &
        rhoarray(i), tarray(j), ylarray(k), mularray(i,j,k), parray(i,j,k), earray(i,j,k), sarray(i,j,k)
      end do rho_write_loop
    end do temp_write_loop
  end do yl_write_loop
  close(iounit)
end program main
