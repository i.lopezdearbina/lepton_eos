module lepeos_mod
  use, intrinsic::iso_fortran_env, dp=>real64
  use constants
  implicit none
contains

  subroutine abundlep (mass, temp, mu, n, npar, nant)
    real(dp), intent(in)  :: mass, temp, mu
    real(dp), intent(out) :: n, npar, nant 
    real(dp) :: f12, f32, feta, fbeta
    real(dp) :: eta, beta, fac
    
    eta = (mu - mass)/temp
    beta = temp/mass
    
    fac = mass*sqrt(beta)/hbarcfm
    fac = sqrt(2.0d0)*fac*fac*fac/(pi*pi)
    
    call dfermi (0.5d0, eta, beta, f12, feta, fbeta)
    call dfermi (1.5d0, eta, beta, f32, feta, fbeta)
    npar = fac * (f12 + beta * f32)
    
    eta = (-mu - mass)/temp
    call dfermi (0.5d0, eta, beta, f12, feta, fbeta)
    call dfermi (1.5d0, eta, beta, f32, feta, fbeta)
    nant = fac * (f12 + beta * f32)
    
    n = npar - nant
  end subroutine abundlep
  
  subroutine preslep (mass, temp, mu, p, ppar, pant)
    real(dp), intent(in)  :: mass, temp, mu
    real(dp), intent(out) :: p, ppar, pant 
    real(dp) :: f32, f52, feta, fbeta
    real(dp) :: eta, beta, fac
    
    eta = (mu - mass)/temp
    beta = temp/mass
    
    fac = mass*beta/hbarcfm
    fac = 2.0d0*mass*mass*sqrt(2.0d0*beta)*fac*fac/(3.0d0*pi*pi*hbarcfm)
    
    call dfermi (1.5d0, eta, beta, f32, feta, fbeta)
    call dfermi (2.5d0, eta, beta, f52, feta, fbeta)
    ppar = fac * (f32 + 0.5d0 * beta * f52)
    
    eta = (-mu - mass)/temp
    call dfermi (1.5d0, eta, beta, f32, feta, fbeta)
    call dfermi (2.5d0, eta, beta, f52, feta, fbeta)
    pant = fac * (f32 + 0.5d0 * beta * f52)
    
    p = ppar + pant
  end subroutine preslep
  
  subroutine enerlep (mass, temp, mu, u, upar, uant)
    real(dp), intent(in)  :: mass, temp, mu
    real(dp), intent(out) :: u, upar, uant 
    real(dp) :: f12, f32, f52, feta, fbeta
    real(dp) :: eta, beta, fac
    
    eta = (mu - mass)/temp
    beta = temp/mass
    
    fac = mass*mass/hbarcfm
    fac = 2.0d0*fac*fac*sqrt(2.0d0*beta)*beta/(2.0d0*pi*pi*hbarcfm)
    
    call dfermi (0.5d0, eta, beta, f12, feta, fbeta)
    call dfermi (1.5d0, eta, beta, f32, feta, fbeta)
    call dfermi (2.5d0, eta, beta, f52, feta, fbeta)
    upar = fac * (f12 + 2.d0*beta*f32 + beta*beta * f52)
    
    eta = (-mu - mass)/temp
    call dfermi (0.5d0, eta, beta, f12, feta, fbeta)
    call dfermi (1.5d0, eta, beta, f32, feta, fbeta)
    call dfermi (2.5d0, eta, beta, f52, feta, fbeta)
    uant = fac * (f12 + 2.d0*beta*f32 + beta*beta * f52)
    
    u = upar + uant
  end subroutine enerlep

  function entrolep (temp, mu, nl, pres, ener)
    real(dp) :: temp, nl, mu, pres, ener, entrolep
    entrolep = (pres + ener + me * nl) / temp
  end function entrolep

end module lepeos_mod
