FC=gfortran
FFLAGS=-g -Wall -fopenmp

OBJS=fermi.o constants.o root_finder_mod.o lepeos_mod.o

main: $(OBJS) main.f90
	$(FC) $(FFLAGS) $^ -o $@

lepeos_loop_calc: $(OBJS) lepeos_loop_calc.f90
	$(FC) $(FFLAGS) $^ -o $@

%.o: %.f*
	$(FC) $(FFLAGS) -c $< -o $@

clean:
	@rm *.mod *.o main lepeos_loop_calc
