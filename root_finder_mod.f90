module root_finder_mod
  ! This module collects all (sub)routines devoted to find
  ! roots of functions. There's no intention to create
  ! commmon or shared variables between them, each of them
  ! thus create an independent unit
  implicit none
contains

  subroutine root_newton (x_i, sub_fnewton, sparams, sroot, istat)
    ! fparams(1): mass
    ! fparams(2): temp
    ! fparams(3): n_in
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: ieee_arithmetic
    use, intrinsic :: ieee_features
    implicit none
    real(dp), intent(in)  :: x_i
    real(dp), intent(in)  :: sparams(:)
    real(dp), intent(out) :: sroot
    integer,  intent(out) :: istat
    integer,  parameter :: iter_max = 100
    real(dp), parameter :: tol = 1.0e-15_dp
    real(dp) :: x, dx, f, df
    integer :: i
    interface
      subroutine sub_fnewton (x, params, fo, dfo)
        import :: dp
        real(dp), intent(in)  :: x, params(:)
        real(dp), intent(out) :: fo, dfo
      end subroutine sub_fnewton
    end interface
    istat = 0
    if (sparams(3) == 0.0_dp) then
       x = - sparams(1)/sparams(2)
       sroot = x
       return
    end if
    x = x_i
    i = 0
    do
      i = i + 1
      call sub_fnewton (x, sparams, f, df)
      dx = f / df
      !write(*,'(i5,4(1x,g0))') i, x, dx, f, df
      if (abs(dx) < tol .and. i < iter_max) then
         sroot = x
         istat = 0
         exit
      else if (i > iter_max) then
         sroot = x
         istat = 1
         write(error_unit,'("WARNING: newton: i > iter_max",i5)') i
         exit
      end if
      x = x - dx
    end do
  end subroutine root_newton

  subroutine newton_lepton (eta_in, params, f, df)
    use,intrinsic::iso_fortran_env, dp=>real64
    use constants
    implicit none
    real(dp), intent(in)  :: eta_in, params(:)
    real(dp), intent(out) :: f, df
    real(dp) :: fac, eta, beta, mass, temp, n_in
    real(dp) :: fe12, fe12eta, fe12beta
    real(dp) :: fe32, fe32eta, fe32beta
    real(dp) :: fp12, fp12eta, fp12beta
    real(dp) :: fp32, fp32eta, fp32beta
    
    mass = params(1)
    temp = params(2); beta = temp/mass
    n_in = params(3)

    fac = mass*sqrt(beta)/hbarcfm
    fac = sqrt(2.0d0)*fac*fac*fac/(pi*pi)
    fac = 1.0_dp/fac
    
    eta = eta_in
    call dfermi (0.5d0, eta, beta, fe12, fe12eta, fe12beta)
    call dfermi (1.5d0, eta, beta, fe32, fe32eta, fe32beta)
    
    eta = - eta - 2.0_dp / beta
    call dfermi (0.5d0, eta, beta, fp12, fp12eta, fp12beta)
    call dfermi (1.5d0, eta, beta, fp32, fp32eta, fp32beta)
    
    f  =  (fe12 - fp12 + beta * (fe32 - fp32)) / (fac * n_in) - 1._dp
    df =  (fe12eta + fp12eta + beta * (fe32eta + fp32eta)) / (fac * n_in)
    !print'(" >>> DEBG:: ",3(1x,g0))', eta_in, f, df
  end subroutine newton_lepton 

end module root_finder_mod
